<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Album extends ObjectModel{
        const VISIBLE_FRONT = 1;        
        const INVISIBLE_FRONT = 0;
       // public $id;

        public $id_gallery;

        public $name;
        
        public $description;
        
        public $date_add;

        public $date_upd;

        public $active;
        
        
        public function __construct($id = null, $id_lang = null, $id_shop = null)        {
            parent::__construct($id, $id_lang, $id_shop);            
       }       
    
        /**
        * @see ObjectModel::$definition
        */
        public static $definition = array(
               'table' => 'ofi_gallery_album',
               'primary' => 'id_album',
               'fields' => array(
                        'id_gallery' =>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
                        'name' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                        'description' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                        'date_add' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                        'date_upd' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                        'active' => 	array('type' => self::TYPE_BOOL)
               ),
        );
    
        public static function getAlbum($data = array(),$full = false){
            $where = '';
            $join ='';
            if($full){
                $join ='LEFT JOIN '._DB_PREFIX_.'ofi_album_product AS apr ON ga.id_album = apr.id_album 
                        LEFT JOIN '._DB_PREFIX_.'ofi_customer_gallery AS cg ON cg.id_gallery = ga.id_gallery';
            }
            if(count($data)){
                $where .= 'WHERE ';
                $last_key = key( array_slice( $data, -1, 1, TRUE ) );
                foreach ($data as $key => $value){
                    if($key != $last_key){
                        $and = ' AND ';
                    }
                    else{
                        $and = '';
                    }
                    $where.= 'ga.'.$key.'="'.$value.'"'.$and ;
                }
                $sql = 'SELECT *
                            FROM `'._DB_PREFIX_.'ofi_gallery_album` AS ga
                            '.$join.'    
                            '. $where.'
                            GROUP BY ga.id_album  
                            ORDER BY ga.`date_add` DESC';
                $result = Db::getInstance()->executeS($sql );
                return $result;
            }
            else{
                $sql = 'SELECT *
                            FROM `'._DB_PREFIX_.'ofi_gallery_album` AS ga '.$join.'
                            GROUP BY ga.id_album  
                            ORDER BY ga.`date_add` DESC' ;
                $result = Db::getInstance()->executeS($sql);
                return $result;
            }
            return false;
        }
        
        public static function getAlbumCustomer($id_album){
            $sql = 'SELECT ga.*, cg.id_customer
                    FROM `'._DB_PREFIX_.'ofi_gallery_album` ga
                    LEFT JOIN '._DB_PREFIX_.'ofi_customer_gallery AS cg ON cg.id_gallery = ga.id_gallery
                    WHERE ga.id_album ='.$id_album;
            
            $result = Db::getInstance()->getRow($sql);
            if(!count($result)){
                return false;
            }
            return $result;
        }
        
        public static function getAlbumProductIds($id_album){
            $result = Db::getInstance()->executeS('
                            SELECT apr.id_product
                            FROM `'._DB_PREFIX_.'ofi_album_product` AS apr
                            WHERE apr.`id_album` = '.$id_album);
            return $result;
        }
        
}
