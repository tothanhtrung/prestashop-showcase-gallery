<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Photo extends ObjectModel
{
        public $id_photo;

        public $id_album;

        public $date_add;

        public $date_upd;

        public $active;
        
        public $verify;
        
        /**
        * @see ObjectModel::$definition
        */
        public static $definition = array(
               'table' => 'ofi_album_photo',
               'primary' => 'id_photo',
               'fields' => array(
                       'id_album' =>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
                       'date_add' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                       'date_upd' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                       'active' => 	array('type' => self::TYPE_BOOL)
               ),
        );
        
        public static function  getPhotos($id_album, $checkActive = true)
	{
		if($checkActive){
                    $condition = ' AND ap.`active` = 1';
                }
                else{
                    $condition ='';
                }
                return Db::getInstance()->executeS('
		SELECT *
		FROM `'._DB_PREFIX_.'ofi_album_photo` ap
		WHERE ap.`id_album` = '.(int)$id_album.$condition.'
                ORDER BY ap.`date_add` DESC');
	}
	
	public static function getPhotoLink($id_photo, $type=null){
            $path = _MODULE_DIR_.'showcasegallery/photos/'.$id_photo.'/';
            if(!$type){
                $link = $path.$id_photo.'.jpg';
            }
            else{
                $link = $path.$id_photo.'-'.$type.'.jpg';
            }
            return $link;
        }
        
}