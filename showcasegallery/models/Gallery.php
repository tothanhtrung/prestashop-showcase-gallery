<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Gallery extends ObjectModel
{
        //public $id;

        public $id_gallery;

        public $id_customer;

        public $date_add;

        public $date_upd;

        public $active;

        /**
        * @see ObjectModel::$definition
        */
        public static $definition = array(
               'table' => 'ofi_customer_gallery',
               'primary' => 'id_gallery',
               'fields' => array(
                       'id_customer' =>	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
                       'date_add' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                       'date_upd' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                       'active' => 	array('type' => self::TYPE_BOOL)
               ),
        );

        public static function getGallery($id_customer){
            $result = Db::getInstance()->getRow('
                            SELECT *
                            FROM `'._DB_PREFIX_.'ofi_customer_gallery` AS cg
                            WHERE cg.`id_customer` ='.(int)$id_customer.'
                            AND cg.`active` = 1'
                    );
            return $result;

        }
        
	public static function getGalleryByCustomerId($id_customer){
            $result = Db::getInstance()->getRow('
                            SELECT *
                            FROM `'._DB_PREFIX_.'ofi_customer_gallery` AS cg
                            WHERE cg.`id_customer` ='.(int)$id_customer.'
                            AND cg.`active` = 1'
                    );
            return $result;

        }
        
        public static function getGalleryById($id_gallery){
            $result = Db::getInstance()->getRow('
                            SELECT *
                            FROM `'._DB_PREFIX_.'ofi_customer_gallery` AS cg
                            WHERE cg.`id_gallery` ='.(int)$id_gallery.'
                            AND cg.`active` = 1'
                    );
            return $result;

        }
}