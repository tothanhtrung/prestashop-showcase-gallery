<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ShowcasegalleryListAlbumsModuleFrontController extends ModuleFrontController
{
        
        public function init()
        {
                parent::init();
                require_once($this->module->getLocalPath().'models/Gallery.php');
                require_once($this->module->getLocalPath().'models/Album.php');
                require_once($this->module->getLocalPath().'models/Photo.php');
                $this->addCSS(_MODULE_DIR_.'showcasegallery/'.'showcasegallery.css');
        }
        /**
         * @see FrontController::initContent()
         */
        public function initContent() {
            parent::initContent();
            $albums = Album::getAlbum(null, true);
            foreach ($albums as $k=>$row){
                $albumId = $row['id_album'];
                $id_customer = Album::getAlbumCustomer($albumId);
                $customer = new Customer($id_customer['id_customer']);
                $albums[$k]['customer'] =  $customer->firstname.' '.$customer->lastname;
                $photo = Photo::getPhotos($albumId);
                if(!empty($photo)){
                    $id_photo = $photo[0]['id_photo'];                    
                    $albums[$k]['photo'] = Photo::getPhotoLink($id_photo,'home_default');
                    $albums[$k]['url'] = $this->context->link->getModuleLink('showcasegallery', 'gallery').'?id_gallery='.$row['id_gallery'].'&?id_album='.$row['id_album'];
                }
            }
            $this->context->smarty->assign('albums', $albums);
            $this->setTemplate('list.tpl');
        }
}