<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ShowcasegalleryAlbumModuleFrontController extends ModuleFrontController
{
        public function init()
        {
            parent::init();

            if(!Context::getContext()->customer->isLogged()){
                    Tools::redirect('index.php?controller=my-account');
            }
            require_once($this->module->getLocalPath().'models/Gallery.php');
            require_once($this->module->getLocalPath().'models/Album.php');
            require_once($this->module->getLocalPath().'models/Photo.php');
            $this->addCSS(_MODULE_DIR_.'showcasegallery/'.'showcasegallery.css');
        }
        /**
         * @see FrontController::initContent()
         */
        public function initContent() {
            parent::initContent();
            $id_customer = $this->context->customer->id;       
            $gallery = Gallery::getGallery($id_customer);
            $customer = new Customer($this->context->customer->id);
            $this->context->smarty->assign('owner', $customer);
            //$productlist = $this->getProductOrderedList();
            $productlist = $this->getAllProduct();
            if(count($productlist)){
                $this->context->smarty->assign('productlist', $productlist);
            }
            
            $id_album = Tools::getValue('id_album');
            if($id_album){
                //update album
                $album = Album::getAlbumCustomer($id_album);
                if(!$album || (int)$album['id_customer'] != $this->context->customer->id){
                    Tools::redirect('index.php?controller=my-account');
                }
                else{
                    // get album product in checked
                    $productIds = Album::getAlbumProductIds($id_album);
                    if(count($productIds)){
                        $this->context->smarty->assign('productIds', $productIds);
                    }
                    //load photos
                    if(isset($id_album)){
                        $photos = Photo::getPhotos($id_album);
                        foreach($photos as $k=>$photo){
                            $photos[$k]['thumb'] = Photo::getPhotoLink($photo['id_photo'], 'home_default');
                            $photos[$k]['raw'] = Photo::getPhotoLink($photo['id_photo']);
                        }
                        $this->context->smarty->assign('photos', $photos);
                    }
                }
                $this->context->smarty->assign('album', $album);
            }
            $this->setTemplate('album.tpl');
        }

        
        public function postProcess()
        {
            if (Tools::isSubmit('Save') || Tools::isSubmit('Update')){
                if(!isset($_POST['photos']) && $_FILES["photo"]["error"] > 0){
                    $errors['photo'] = Tools::displayError('Error when upload photo');
                    $this->context->smarty->assign('error', $errors);
                }
                else{
                    $photo = $_FILES["photo"];
                    $this->processSaveAlbum($_POST,$photo);
                }
                    
            }
            if (Tools::getValue('action') == 'delete'){
                $this->processDeleteAlbum(Tools::getValue('id_album'));
            }
        }

        
        public function processSaveAlbum($data,$photo)
        {
            $errors = array();
            //check input fields
            if(empty($data['title'])){
                $errors['title'] = Tools::displayError('Title is required');
            }
            if(empty($data['description'])){
                $errors['description'] = Tools::displayError('Description is required');
            }
            if(empty($data['product']) || !is_array($data['product'])){
                $errors['product'] = Tools::displayError('Product is required');
            }
            
            if(!count($errors)){
                // save album
                $gallery = Gallery::getGallery($this->context->customer->id);
                $id_album = Tools::getValue('id_album');
                if($id_album){
                    $album = new Album($id_album);
                }
                else{
                    $album = new Album;
                }
                $album->id_gallery = $gallery['id_gallery'];
                $album->name = $data['title'];
                $album->description = $data['description'];
                $album->active = 1;
                if($id_album){
                    $album->update();
                }
                else{
                    $album->save();
                }
                // save album product
                $res = $this->insertAlbumProduct($data['product'], $album->id);
                //save photo
                if($_FILES['photo']['error']>0){
                    $errors['photo'] = Tools::displayError('Error when upload photo');
                }
                else{
                    $photos = new Photo();
                    $photos->id_album = $album->id;
                    $photos->active = 1;
                    $photos->save();
                    //upload photo
                    $image = new Image($photos->id);
                    $method = 'auto';
                    if (!$new_path =$this->createPhotoFolder($photos->id))
                            $errors[] = Tools::displayError('An error occurred during new folder creation');
                    if (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['photo']['tmp_name'], $tmpName))
                            $errors[] = Tools::displayError('An error occurred during the image upload');
                    elseif (!ImageManager::resize($tmpName, $new_path.$photos->id.'.'.$image->image_format))
                            $errors[] = Tools::displayError('An error occurred while copying image.');
                    elseif ($method == 'auto')
                    {
                            $imagesTypes = ImageType::getImagesTypes('products');
                            foreach ($imagesTypes as $k => $image_type)
                            {
                                    if (!ImageManager::resize($tmpName, $new_path.$photos->id.'-'.stripslashes($image_type['name']).'.'.$image->image_format, $image_type['width'], $image_type['height'], $image->image_format))
                                            $errors[] = Tools::displayError('An error occurred while copying image:').' '.stripslashes($image_type['name']);
                            }
                    }
                    @unlink($tmpName);
                }
                $this->context->smarty->assign('id_album', $album->id);
                Tools::redirect($this->context->link->getModuleLink('showcasegallery', 'album').'?id_album='.$album->id);
            }
            else{
                $this->context->smarty->assign('error', $errors);
            }
        }
        
        public function createPhotoFolder($id_photo)
	{
		$path = _PS_MODULE_DIR_.'showcasegallery/photos/'.$id_photo.'/';
                $success = @mkdir($path, 0777, true);
                $chmod = @chmod($path,0777);
                if($success || $chmod){
                    return $path;
                }
                return false;
	}
        
        
        
        public function processDeleteAlbum($id_album){
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ofi_gallery_album` WHERE `id_album` = '.(int)$id_album);
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ofi_album_product` WHERE `id_album` = '.(int)$id_album);
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ofi_album_photo` WHERE `id_album` = '.(int)$id_album);
            Tools::redirect(Tools::redirect($this->context->link->getModuleLink('showcasegallery', 'gallery')));
        }
        
        public function insertAlbumProduct($products,$id_album){
            $this->deleteAlbumProduct($id_album);
            foreach ( $products as $id_product)
                $ids[] = '('.$id_album.','.(int)$id_product.')';
            $res = Db::getInstance()->execute('INSERT INTO '._DB_PREFIX_.'ofi_album_product (id_album,id_product) VALUES '.implode(',', $ids));
            return $res;
        }
        
        public function deleteAlbumProduct($id_album){
            return Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ofi_album_product` WHERE `id_album` = '.(int)$id_album);
        }
        

        public function getAllProduct(){
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'product` p 
                    LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
                                p.`id_product` = pl.`id_product`
                                AND pl.`id_lang` = '.(int)$this->context->language->id.'
                        )
                    WHERE p.active=1 LIMIT 1000'; 
            
            $products = Db::getInstance()->ExecuteS( $sql );
            $products = Product::getProductsProperties($this->context->language->id,$products);
            return $products;
        }
        
        public function getProductOrderedList(){
            $customer = new Customer($this->context->customer->id);
            $productsBought = $customer->getBoughtProducts();
            $products = array();
            if($total = count($productsBought)){
                foreach($productsBought as $k){
                    $products[$k['product_id']] = $k['product_name'];
                }
                $products  = array_unique($products,SORT_REGULAR);
            }
            return $products;
        }
        
        
}