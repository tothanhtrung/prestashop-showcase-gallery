<?php
    class ShowcasegalleryGalleryModuleFrontController extends ModuleFrontController
    {
        
        public function init()
	{
		parent::init();
		require_once($this->module->getLocalPath().'models/Gallery.php');
                require_once($this->module->getLocalPath().'models/Album.php');
                require_once($this->module->getLocalPath().'models/Photo.php');
                $this->addCSS(_MODULE_DIR_.'showcasegallery/'.'showcasegallery.css');
	}
        
        public function initContent() {
            parent::initContent();
            //check permission
            $id_customer = $this->context->customer->id;            
            $id_gallery = Tools::getValue('id_gallery');
            $isOwner = false;
            if($id_gallery){
                //check gallery exist
                $gallery = Gallery::getGalleryById($id_gallery);
                if(!$gallery){
                     Tools::redirect('index.php?controller=my-account');
                }
                $isOwner = $this->checkIsOwner($id_gallery, $id_customer);
                $albums = $this->getAlbum($id_gallery);
                $owner = $this->getCustomer($gallery['id_customer']);
            }
            else{
                //create gallery if not exist
                if (!Context::getContext()->customer->isLogged())
                    Tools::redirect('index.php?controller=authentication&redirect=module&module=favoriteproducts&action=account');
                $gallery = $this->customerGallery($id_customer,1);
                $albums = $this->getAlbum($gallery['id_gallery']);
                $owner = $this->getCustomer($gallery['id_customer']);
                $isOwner = true;
            }
            $id_album = Tools::getValue('id_album');
            if(count($albums)){
                foreach ($albums as $k=>$album){
                    $getAlbumThumbnailList = $this->getAlbumPhotobyType($albums[$k]['id_album'],'home_default');
                    $albums[$k]['thumbnail'] = reset($getAlbumThumbnailList);
                }
                $this->context->smarty->assign('albums', $albums);
                //get newest album or request album
                if(!$id_album) {
                    $album = reset($albums);
                    $id_album = $album['id_album'];
                }
                $default = $this->getDefaultAlbum($id_album);
                $products = $this->getAlbumProducts($id_album);
                if(!$default || !count($products)){
                    Tools::redirect('index.php?controller=my-account');
                }

                $this->context->smarty->assign('id_album', $id_album);
                $this->context->smarty->assign('default', $default);
                $this->context->smarty->assign('products', $products);
            }
            $this->context->smarty->assign('owner', $owner);
            $this->context->smarty->assign('isOwner', $isOwner);
            
            $this->setTemplate('gallery.tpl');
        }
        
        public function customerGallery($id_customer, $active = 0){
            $hasGallery = Gallery::getGallery($id_customer);
            if(!$hasGallery)
            {
                $gallery = new Gallery();
		$gallery->id_customer = $id_customer;
		$gallery->active = $active;
                if (!$gallery->add()){
                    $error = 'cannot create gallery';
                    return false;
                }
                return Gallery::getGallery($id_customer);
            }
            else{
                return $hasGallery;
            }
        }
        
        public function postProcess(){
            if(Tools::getValue('action') == 'loadAlbum'){
                if(Tools::getValue('id_album')){
                    $this->processLoadAlbum(Tools::getValue('id_album'));
                }
            }
            
        }
        
        public function getCustomer($id_customer){
           $customer = new Customer($id_customer);
           return $customer;
        } 
        
        public function checkIsOwner($id_gallery, $id_customer){
            $galleryObj = new Gallery($id_gallery);
            if($galleryObj->id_customer == $id_customer ){
                return true;
            }
            return false;
        }
        
        public function getAlbum($id_gallery){
            $albums = Album::getAlbum(array('id_gallery' => $id_gallery, 'active' => 1));
            return $albums;
        }
        
        public function getAlbumPhotobyType($id_album, $type = null){
            $photos = Photo::getPhotos($id_album,false);
            $photoThumbnail = array();
            foreach ($photos as $photo){
                $photoThumbnail[] = Photo::getPhotoLink($photo['id_photo'],$type);
            }
            return $photoThumbnail;
            
        }
        
        
        public function getDefaultAlbum($id_album){
            $album = Album::getAlbum(array('id_album' => $id_album, 'active' => 1));
            if($album){
                $album = reset($album);
                $main_photos = $this->getAlbumPhotobyType($id_album,'thickbox_default');
                $album['main'] = reset($main_photos);
                $photos = Photo::getPhotos($id_album,false);
                foreach($photos as $photo){
                    $album['photos'][$photo['id_photo']]['link']= Photo::getPhotoLink($photo['id_photo'],'home_default');
                }
                return $album;
            }
            return false;
        }
        
        public function processLoadAlbum($id_album){
            $photos = Photo::getPhotos($id_album);
            $album = Album::getAlbum(array('id_album' => $id_album, 'active' => 1));
            foreach($photos as $k=>$photo){
                $photo_list[$k]['link']= Photo::getPhotoLink($photo['id_photo'], 'home_default');
                $photo_list[$k]['id']= $photo['id_photo'];
            }
            $main_photo = reset($photos);
            $products = $this->getAlbumProducts($id_album);
            foreach($products as $product){
                $product_list[$product['id_product']]['link'] = $product['link'];
                $product_list[$product['id_product']]['image'] = $this->context->link->getImageLink($product['link_rewrite'], $product['id_image'], 'home_default');
                $product_list[$product['id_product']]['name'] = $product['name'];
                
            }
            if($photos){
                $json = array(
                    'status' =>'ok',
                    'album'=> $album,
                    'main'=>Photo::getPhotoLink($main_photo['id_photo'],'thickbox_default'),
                    'photo_list' => $photo_list,
                    'product_list'=> $product_list
                );
                die(Tools::jsonEncode($json));
            }
            
        }
        
        public function getAlbumProducts($id_album){
            $products = array();
            $result = Db::getInstance()->executeS('
                            SELECT apr.id_product
                            FROM `'._DB_PREFIX_.'ofi_album_product` AS apr
                            WHERE apr.`id_album` = '.$id_album);
            if(count($result)){
                foreach($result as $k=>$productId){
                    $products[$k] = $this->getProductbyId($productId['id_product']) ;
                }
            }
            return $products;
            
        }
       
        public function getProductbyId($id_product){
            $sql = 'SELECT p.*, product_shop.*, stock.`out_of_stock` out_of_stock, pl.`description`, pl.`description_short`,
						pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`,
						p.`ean13`, p.`upc`, image_shop.`id_image`, il.`legend`
					FROM `'._DB_PREFIX_.'product` p
					LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
						p.`id_product` = pl.`id_product`
						AND pl.`id_lang` = '.(int)$this->context->language->id.'
					)
					'.Shop::addSqlAssociation('product', 'p').'
					LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
					Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
					LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$this->context->language->id.')
					'.Product::sqlStock('p', 0).'
					WHERE p.id_product = '.(int)$id_product.'
					AND (i.id_image IS NULL OR image_shop.id_shop='.(int)$this->context->shop->id.')';
            $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
            return Product::getProductProperties($this->context->shop->id, $row);
        }
        
    }