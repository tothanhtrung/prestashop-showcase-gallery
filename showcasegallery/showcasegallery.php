<?php 
if (!defined('_PS_VERSION_'))
	exit;

include_once dirname(__FILE__).'/models/Album.php';
include_once dirname(__FILE__).'/models/Gallery.php';
include_once dirname(__FILE__).'/models/Photo.php';
class ShowcaseGallery extends Module
{
    public function __construct() {
        $this->name = 'showcasegallery';
        $this->version = '0.1';
        $this->author = 'Officience';
        $this->need_instance = 0;
        
        parent::__construct();
        
        $this->displayName = $this->l('Showcase Gallery');
        $this->description = $this->l('Create gallery for customer');
        
    }
    
    public function install() {
        if(parent::install() == false 
            || !$this->registerHook('displayCustomerAccount') 
            || !$this->registerHook('displayFooterProduct')
                ){
            return false;
        }
        if(!Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ofi_customer_gallery` (
                                            `id_gallery` int(11)unsigned NOT NULL AUTO_INCREMENT,
                                            `id_customer` int(11),
                                            `date_add` datetime NOT NULL,
                                            `date_upd` datetime NOT NULL,
                                            `active` tinyint(4) NOT NULL DEFAULT \'0\',
                                            PRIMARY KEY (`id_gallery`)) 
                                            ENGINE=MyISAM DEFAULT CHARSET=utf8')||
                
                !Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ofi_gallery_album` (
                                            `id_album` int(11)unsigned NOT NULL AUTO_INCREMENT,
                                            `id_gallery` int(11) unsigned NOT NULL,
                                            `name` varchar(64) NULL DEFAULT NULL,
                                            `description` text NULL DEFAULT NULL,
                                            `date_add` datetime NOT NULL,
                                            `date_upd` datetime NOT NULL,
                                            `active` tinyint(4) NOT NULL DEFAULT \'0\',
                                            PRIMARY KEY (`id_album`)) 
                                            ENGINE=MyISAM DEFAULT CHARSET=utf8')||
                
                !Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ofi_album_photo` (
                                            `id_photo` int(11)unsigned NOT NULL AUTO_INCREMENT,
                                            `id_album` int(11) unsigned NOT NULL,
                                            `date_add` datetime NOT NULL,
                                            `date_upd` datetime NOT NULL,
                                            `active` tinyint(4) NOT NULL DEFAULT \'0\',
                                            PRIMARY KEY (`id_photo`)) 
                                            ENGINE=MyISAM DEFAULT CHARSET=utf8')||
                
                !Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ofi_album_product` (
                                            `id_album` int(11) unsigned NOT NULL,
                                            `id_product` int(11)unsigned NOT NULL,
                                            PRIMARY KEY (`id_album`, `id_product`)) 
                                            ENGINE=MyISAM DEFAULT CHARSET=utf8')
                ){
            return false;
            
        }
        return true;
        
    }
    
    public function uninstall() {
        $this->deletePhotosFolder(_PS_MODULE_DIR_.'showcasegallery/photos');
        if(!parent::uninstall()
                || !Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ofi_customer_gallery`') 
                || !Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ofi_gallery_album`')
                || !Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ofi_album_photo`')
                || !Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ofi_album_product`') 
            )
        {
            return false;
        }
        return true;
    }
    
    public function deletePhotosFolder($dirPath){
        if (! is_dir($dirPath)) {
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deletePhotosFolder($file);
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }
    
    
    // add gallery in my account
    public function hookDisplayCustomerAccount($params)
    {
        return $this->display(__FILE__, 'my-account.tpl');
    }
    
    public function checkCustomerhasGallery($id_customer){
            $result = Db::getInstance()->getRow('
			SELECT *
			FROM `'._DB_PREFIX_.'ofi_customer_gallery` AS cg
                        LEFT JOIN '._DB_PREFIX_.'ofi_gallery_album AS ga ON cg.id_gallery = ga.id_gallery
			WHERE cg.`id_customer` ='.(int)$id_customer.'
                        AND cg.`active` = 1 AND ga.`active`'
		);
            return $result;
    }
    /**
     * Implement hook DisplayFooterProduct 
     */
    public function hookDisplayFooterProduct($params){
        $this->context->controller->addCSS(($this->_path) . 'showcasegallery.css', 'all');
        $this->context->controller->addCSS(($this->_path) . 'skin.css', 'all');
        $this->context->controller->addJS(($this->_path) . 'jquery-1.9.1.min.js');
        $this->context->controller->addJS(($this->_path) . 'jquery.jcarousel.min.js');
        
        $product = $params['product'];
        $sql =  ' SELECT ga.* FROM '. _DB_PREFIX_ .'ofi_album_product AS ap';
        $sql .= ', ' . _DB_PREFIX_ .'ofi_gallery_album AS  ga';
        $sql .= ' WHERE ga.active = 1 AND ap.id_album = ga.id_album AND id_product = ' . $product->id;
        $sql .= ' ORDER BY ga.`date_add` DESC';
        
        $albums = array();
        $total = 0;
        if ($results = Db::getInstance()->ExecuteS($sql)){            
            foreach ($results as $row){
                $total++;
                $albumId = $row['id_album'];
                $photo = Photo::getPhotos($row['id_album']);
                $id_customer = Album::getAlbumCustomer($row['id_album']);
                $customer = new Customer($id_customer['id_customer']);
                $row['customer'] =  $customer->firstname.' '.$customer->lastname;
                
                if(!empty($photo)){
                    $id_photo = $photo[0]['id_photo'];                    
                    $row['photo'] = Photo::getPhotoLink($id_photo,'home_default');
                    $row['url'] = $this->context->link->getModuleLink('showcasegallery', 'gallery').'?id_gallery='.$row['id_gallery'].'&?id_album='.$row['id_album'];
                }
                
                $albums[] = $row;
            }
        }
        $this->context->smarty->assign(
            array(
                'total'=>$total,
                'albums' => $albums,
                'product' => $product
            )
        );
        
        return $this->display(__FILE__, 'albums.tpl');
    }
}

	
	
	
