<div id="list-last-albums" class="block bar_present">
    <h3 class="title_block">{l s='Showcase Gallery' mod='showcasegallery'}</h3>
    {if $total != 0}
        <ul class="ul-last-album">
            {foreach from=$albums key=key item=album}
                    <li class="item-album">
                        <a class="album" href="{$album.url}"> <img src="{$album.photo}" alt=""/>  </a>
                        <p><a class="name_user" href="{$album.url}"><span>{l s='By: ' mod='showcasegallery'}</span>{$album.customer}</a></p>
                        <p><a class="title_alb" href="{$album.url}">{$album.name|truncate:20:'...'|escape:html:'UTF-8'}</a></p>
                    </li>
            {/foreach}
        </ul>
        <div class="clearfix"></div>
    {/if}
    {if $total > 0}
        <a class="view-more" href="{$link->getModuleLink('showcasegallery', 'listalbums')}" title="{l s='More' mod='showcasegallery'}">{l s='More' mod='showcasegallery'}</a>
    {/if}

</div>