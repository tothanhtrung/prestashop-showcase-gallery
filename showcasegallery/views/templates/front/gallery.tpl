<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

{$owner_name = "`$owner->firstname` `$owner->lastname`"}
{capture name=path} <a href="{$link->getModuleLink('showcasegallery', 'listalbums')|escape:'htmlall':'UTF-8'}"> {l s='Gallery' mod='showcasegallery'}</a> <span class="navigation-pipe">{$navigationPipe}</span>{$owner_name}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}
<div id="My-gallery"> {if $isOwner}
    <h1>{l s='My Gallery' mod='showcasegallery'}</h1>
    {else}
    <h1>{$owner_name}{l s=' Gallery' mod='showcasegallery'}</h1>
    {/if}
    {if $isOwner} <a href="{$link->getModuleLink('showcasegallery', 'album')}" title="{l s='Add new album' mod='showcasegallery'}" class="button clearfix">{l s='Add new album' mod='showcasegallery'}</a> {/if}
    <div  class="clearfix"></div>
    <div id="bar_present" class="bar_present"> {if isset($albums)}
        <ul id="album_list">
            {foreach from=$albums item=album}
            <li id="{$album['id_album']}" class="photo_thumb"> <img src="{$album['thumbnail']}" alt="{$album['name']|escape:html:'UTF-8'}" /> {if $isOwner}
                <div> <a class='edit' title="edit" href="{$link->getModuleLink('showcasegallery', 'album')}?id_album={$album['id_album']}"> <img src="{$modules_dir}showcasegallery/img/edit.gif"  /> </a> <a class='del' title="delete" onclick="deleteAlbum({$album['id_album']})"> <img src="{$modules_dir}showcasegallery/img/del.gif"  /></a> </div>
                {/if}
                <p>{$album['name']|escape:html:'UTF-8'}</p>
            </li>
            {/foreach}
        </ul>
        {else}
        <div>{l s='No album added in this gallery yet. Please add new album ' mod='showcasegallery'}</div>
        {/if} </div>
    {if isset($default)}
    <div id="album_content" class="block" >
        <h3 id="album_name" class="title_block">{$default['name']}</h3>
        <div id="album_main"><img src="{$default['main']}" /></div>
        <div id="photo_list">
            <ul id="ListImages" class="thumb_list">
                {if isset($isOwner) && $isOwner}
                {foreach from=$default['photos'] key=k item=photo}
                <li id="photo_{$k}"> <img src="{$photo.link}"> </li>
                {/foreach}
                {/if}
            </ul>
        </div>
        <div id="album_des">{$default['description']} </div>
    </div>
    <div class="clearfix"></div>
    {if isset($products)}
    <div id="product_block" class="block"> <h3 class="title_block">{l s='Related Product' mod='showcasegallery'}</h3>
        <ul>
            {foreach from=$products item=product}
            <li>
                <div><a href="{$product.link}"> <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')}" alt="{$product.name}" /> </a> <a href="{$product.link}" title="{$product.name}">{$product.name}</a> </div>
            </li>
            {/foreach}
        </ul>
    </div>
    {/if}
    {/if} </div>
<script>
    var gallery = '{$link->getModuleLink('showcasegallery', 'gallery')}';
    var photo_folder = {$smarty.const.__PS_BASE_URI__} + "modules/showcasegallery/photos/";
	
    function deleteAlbum(id_album){
                    if (confirm("{l s='Do you realy want to delete this album ?' mod='showcasegallery' js=1}")){
                        window.location = "{$link->getModuleLink('showcasegallery', 'album')}?id_album=" + id_album+"&action=delete" ;
                    }
                }
    $('document').ready(function(){
            $('.photo_thumb').click(function(){
                    var id_album =  $(this).attr('id');
                    $.ajax({
                            url: gallery,
                            type: "POST",
                            data: {
                                    action: "loadAlbum",
                                    id_album: id_album,
                                    ajax: 1
                            },
                            dataType : 'json',
                            success: function(responseJSON){
                                if(responseJSON.status =='ok'){
                                    loadAlbum(responseJSON.album,responseJSON.main,responseJSON.photo_list,responseJSON.product_list);
                                }
                            }
                    });
            });

            function loadAlbum(album,main,photo_list,product_list){
                $("#album_name").html(album[0].name);
                $("#album_main img").attr("src",main);
                $("#album_des").html(album[0].description);
                $("#photo_list ul li").remove();
                $.each(photo_list, function(id, item){
                    $("#photo_list ul").append("<li id='photo_"+item.id+"'><img src=\""+ item.link +"\"></li>");
                });
                $("#product_block ul li").remove();
                $.each(product_list, function(id, item){
                    $("#product_block ul").append("<li><div><a href='"+ item.link +"'><img src='"+ item.image +"' alt='"+item.name+"'/></a><a href='"+item.link+"' title='"+item.name+"'>"+item.name+"</a></div></li>");
                });
            };

            $("#photo_list ul li").die().live('click', function(e)
            {
                    e.preventDefault();
                    id = $(this).attr('id').substring(6);
                    var main_photo = photo_folder + '/'+id+'/'+id+'-thickbox_default.jpg'
                    $("#album_main img").attr("src",main_photo);
            });

    })
	
	


</script> 