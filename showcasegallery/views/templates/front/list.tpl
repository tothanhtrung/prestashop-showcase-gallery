{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    {l s='Gallery' mod='showcasegallery'}
{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
<div id="block-last-album" class="bar_present">
    {if !count($albums)}
    	<h3 class="title_block">There is No album</h3>
    {else}
        {if count($albums) == 1}<h3 class="title_block">{l s='There is 1 album' mod='showcasegallery'}</h3>
        {else}
        <h3 class="title_block">{l s='There are %d albums.' sprintf=count($albums) mod='showcasegallery'}</h3>
        {/if}
        <ul>
        {foreach from=$albums key=key item=album}
                <li class="item-album">
                    <a class="album" href="{$album.url}"> <img src="{$album.photo}" alt=""/>  </a>
                    <p><i>{l s='By: ' mod='showcasegallery'}</i><a class="name_user" href="{$album.url}">{$album.customer}</a></p>
                    <p><a class="title_alb" href="{$album.url}"><b>{$album.name|truncate:20:'...'|escape:html:'UTF-8'}</b></a></p>
                </li>
        {/foreach}
        </ul>
    {/if}
</div>