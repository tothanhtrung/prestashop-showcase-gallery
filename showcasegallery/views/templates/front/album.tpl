{$owner_name = "`$owner->firstname` `$owner->lastname`"}
{capture name=path}
	<a href="{$link->getModuleLink('showcasegallery', 'listalbums')|escape:'htmlall':'UTF-8'}">{l s='Gallery' mod='showcasegallery'}</a>
        <span class="navigation-pipe">{$navigationPipe}</span><a href="{$link->getModuleLink('showcasegallery', 'gallery')|escape:'htmlall':'UTF-8'}">{$owner_name}</a>
        <span class="navigation-pipe">{$navigationPipe}</span>{l s='Add Album' mod='showcasegallery'}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}
<div id="add-album">
<h1 class=""><span>{l s='Add new album' mod='showcasegallery'}</span></h1>
{if isset($error)}
	<div class="error">
		<ul style="list-style:none">
		{foreach from=$error item=msg }
			<li>{$msg}</li>
		{/foreach} 
		</ul>
	</div>
{/if}  
<form action="{$link->getModuleLink('showcasegallery', 'album')}{if isset($album)}?id_album={$album.id_album}{/if}" method="post"  enctype="multipart/form-data" class="add-Alb">
    <fieldset>
        <p class="text">
            <label for="title">{l s='Album title' mod='showcasegallery'}</label>
            <input type="text" name="title" value="{if isset($smarty.post.title)}{$smarty.post.title}{else}{if isset($album) && isset($album.name)}{$album.name}{/if}{/if}" />
        </p>
        
        <p class="text">
            <label>{l s='Album description' mod='showcasegallery'}</label>
            <textarea name="description" value="">{if isset($smarty.post.description)}{$smarty.post.description}{else}{if isset($album) && isset($album.description)}{$album.description}{/if}{/if}</textarea>
        </p>
        <div class="clearfix"></div>
        {if isset($productlist)}
        <p class="text">
                <label for="product">{l s='Related Product' mod='showcasegallery'}</label>
                <span>
                        {foreach from=$productlist key=k item=product}
                                <input type="checkbox" name="product[{$product.id_product}]" value="{$product.id_product}" 
                                    {if isset($smarty.post.product) && isset($smarty.post.product.$k)} checked="checked" 
                                    {else}
                                        {if isset($productIds)}
                                            {foreach from=$productIds item=id}
                                                {if $id.id_product == $product.id_product } checked="checked" {/if}
                                            {/foreach}
                                        {/if}
                                    {/if}/>
                                <label for="product[{$product.id_product}]">{$product.name}</label><br />
                        {/foreach}
                </span>
        </p>
        {/if}
        <p >
                <label >{l s='Photo' mod='showcasegallery'}</label>
                <span>
                    <input type="file" name="photo" size="40" />
                </span>
        </p>
        {if isset($photos)}
        <div class="photo-lst">
            <ul>
            {foreach from=$photos item=photo}
                <li >
                    <a  href="{$photo.raw}" target="_blank">
                        <img title="{$photo.id_photo}" alt="{$photo.id_photo}" src="{$photo.thumb}">
                    </a>
                    <input type="hidden" name="photos[{$photo.id_photo}]" value="{$photo.id_photo}" />
                </li>
            {/foreach}
            </ul>
        </div>
        {/if}
	<p class="submit">
            <label></label>
            <a class="button" href="{$link->getModuleLink('showcasegallery', 'gallery')}" title="{l s='Back to Gallery' mod='showcasegallery'}">{l s='Back to Gallery' mod='showcasegallery'}</a>
            <input type="submit" class="button" name="Save" value="{l s='Save' mod='showcasegallery'}" />
        </p>	
    </fieldset>
</form>
</div>
